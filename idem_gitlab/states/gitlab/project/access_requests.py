"""
Autogenerated using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__

Access requests
"""
from typing import Any
from typing import Dict

import dict_tools.differ as differ


async def present(hub, ctx, name: str, project_id: int) -> Dict[str, Any]:
    r"""
    **Autogenerated function**

    Requests access for the authenticated user to a group or project.


    Args:
        name(Text): The identifier for this state.
        project_id(int): The ID or URL-encoded path of the project owned by the authenticated user.

    Returns:
        Dict[str, Any]

    Examples:

        .. code-block:: sls

            resource_is_present:
              gitlab.project.access_requests.present:
                - name: value
                - project_id: value
    """

    result = dict(comment="", changes=None, name=name, result=True)

    before = hub.exec.request.json.get(
        ctx,
        url=f"{ctx.acct.endpoint_url}/projects/{project_id}/access_requests",
        data={},
        success_codes=[200],
    )
    if before["status"]:
        result["comment"] = f"'{name}' already exists"
    else:
        ret = await hub.exec.request.json.post(
            ctx,
            success_codes=[201, 304, 204],
            url=f"{ctx.acct.endpoint_url}/projects/{project_id}/access_requests",
            **{},
        )
        result["result"] = ret["status"]
        if not result["result"]:
            result["comment"] = ret["comment"]
            return result
        result["comment"] = f"Created '{name}'"

    # Now that the resource exists, update it
    ret = await hub.exec.request.json.put(
        ctx,
        url=f"{ctx.acct.endpoint_url}/projects/{project_id}/access_requests",
        success_codes=[200, 204, 304],
    )

    if not ret["status"]:
        result["status"] = False
        result["comment"] = f"Unable to update '{name}': {ret['comment']}"

    after = hub.exec.request.json.get(
        ctx,
        url=f"{ctx.acct.endpoint_url}/projects/{project_id}/access_requests",
        data={},
        success_codes=[200],
    )
    result["changes"] = differ.deep_diff(before["ret"], after["ret"])
    return result


async def absent(hub, ctx, name: str, project_id: int, user_id: int) -> Dict[str, Any]:
    r"""
    **Autogenerated function**

    Requests access for the authenticated user to a group or project.


    Args:
        name(Text): The identifier for this state.
        project_id(int): The ID or URL-encoded path of the project owned by the authenticated user.
        user_id(int): The user ID of the access requester.

    Returns:
        Dict[str, Any]

    Examples:

        .. code-block:: sls

            resource_is_absent:
              gitlab.project.access_requests.absent:
                - name: value
                - project_id: value
                - user_id: value
    """

    result = dict(comment="", changes=None, name=name, result=True)
    before = hub.exec.request.json.get(
        ctx,
        url=f"{ctx.acct.endpoint_url}/projects/{project_id}/access_requests",
        data={},
        success_codes=[204, 304, 404],
    )

    if before["status"]:
        result["comment"] = f"'{name}' already absent"
    else:
        ret = await hub.exec.request.json.delete(
            ctx,
            url=f"{ctx.acct.endpoint_url}/projects/{project_id}/access_requests",
            success_code=[204],
            **{"user_id": user_id},
        )
        result["result"] = ret["status"]
        if not result["result"]:
            result["comment"] = ret["comment"]
            return result
        result["comment"] = f"Deleted '{name}'"

    after = hub.exec.request.json.get(
        ctx,
        url=f"{ctx.acct.endpoint_url}/projects/{project_id}/access_requests",
        data={},
        success_codes=[204, 304, 404],
    )

    result["changes"] = differ.deep_diff(before["ret"], after["ret"])
    return result


async def describe(hub, ctx) -> Dict[str, Dict[str, Any]]:
    r"""
    **Autogenerated function**

    Describe the resource in a way that can be recreated/managed with the corresponding "present" function


    Gets a list of access requests viewable by the authenticated user.



    Returns:
        Dict[str, Any]

    Examples:

        .. code-block:: bash

            $ idem describe gitlab.project.access_requests
    """

    result = {}

    async for project in hub.exec.gitlab.request.paginate(
        ctx, url=f"{ctx.acct.endpoint_url}/projects"
    ):
        project_id = project["id"]

        async for ret in hub.exec.gitlab.request.paginate(
            ctx, url=f"{ctx.acct.endpoint_url}/projects/{project_id}/access_requests"
        ):
            result[f"/projects-{project_id}-project.access_requests-{ret['id']}"] = {
                "gitlab.project.access_requests.present": [
                    {"project_id": ret.get("id")},
                ]
            }

    return result
